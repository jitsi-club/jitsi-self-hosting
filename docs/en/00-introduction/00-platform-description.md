# Platform Description

>**NOTE** This document is WIP. It may contain inaccurate or incomplete information. Take it as a starting point for discussion.

## What is Jitsi?

Jitsi is a videoconferencing platform (a web application that does *not* require to install any client software app or plugin).

It is available as a free service provided by [8x8] at <https://meet.jit.si>, but since it is FLOSS (Apache 2.0 License) anyone can self-host it; it can be deployed in minutes even on small servers with no particular hardware requirements; only good bandwidth is required.[^stream]

  [^stream]: audio/video streams are not processed but just passed through, and with only 2 participants there is not even a pass-through (audio/video streams directly between users' browsers).

[8x8]: <https://www.8x8.com>

## Business model and people behind it

> Most of the active jitsi-meet maintainers are working for a company where we develop a video conferencing solution, meet.jit.si is a free service, which the developers use to improve with every new version and monitor in order to spot problems and add new features.

(source: <https://community.jitsi.org/t/information-about-jitsi/15426/2>)

The company is [8x8], based in California, with subsidiaries in UK, Romania and Australia.[^atlassian]

This is their [privacy notice](https://www.8x8.com/terms-and-conditions/privacy-policy)

  [^atlassian]: Jitsi was previously owned by Atlassian, who bought it and its owner BlueJimp in 2015, but in 2018 Atlassian decided to sell it to 8x8, to focus on other things (source: <https://techcrunch.com/2018/10/29/atlassian-sells-jitsi-an-open-source-videoconferencing-tool-it-acquired-in-2015-to-8x8/>)

## General notes on architecture (only compliance-relevant aspects)

### Personal Data

Jitsi developers claim that the default deployment does not store any personal information, because "everything stays on the client browsers" (source: <https://github.com/jitsi/jitsi-meet/issues/3950>). This is true as to conversation contents and participants' names and email addresses shared during the conversation; but it does not hold for browsing data and metadata, which *are* actually stored in webserver and Jitsi logs (registered and unregistered users' accesses, connection data - IP address, browser type, pages -- i.e. conference rooms -- visited, etc.). Besides, even if you redirect all the logs to /dev/null (which is not a security-wise practice, anyway), to operate the service you still process data relating to identified or identifiable natural persons.

### Authentication

In the default deployment authentication is disabled: whoever can open a new room and invite people; room access may be protected by a **single password** by the moderator, but it is not mandatory; **by default rooms are opened to anyone**.

<!-- NOTE:2000 Personal data are processed, at least, by moderator and participants -->  

User authentication may be added in various ways; authentication control belongs into the XMPP server on which Jitsi relies/depends ([Prosody]). [Prosody] has a simple built-in authentication system (with passwords stored in plain text or hashed, depending on the module used); many other authentication services can be implemented through dedicated [Prosody] modules.  

In any case, jitsi-meet provides a login form and profile settings where users can set their displayed name and email, but **it does *not* provide any GUI to change user password**, despite XMPP protocol API would allow it (as Jitsi can be integrated - and, well, it is mainly intended to be integrated - in larger applications which supply their own user management GUI).

In Jitsi official documentation, the suggested way to set user passwords is using `prosodyctl` command, that can be run only by the sysadmin. Unfortunately, **this is *not* GDPR-compliant**, because "enabling users to set their password without the admin knowing it" is a basic and unavoidable security measure.

However, since Prosody server is accessible independently from Jisti-meet, the solution is ask users to change their password through an external [XMPP client], like [Pidgin]. In this way, basic GDPR compliance can be achieved.

The only issue is that **in the default Jitsy deployment** (quick install) **prosody uses self-signed certificates**, so XMPP clients may display error messages (or even refuse to connect). Therefore **some extra manual configuration is needed**, (covered in the [howto](../01-deployment-howto/01-authentication.md)).

[XMPP client]: <https://xmpp.org/software/clients.html>
[Pidgin]: <https://pidgin.im/>
[prosody]: https://prosody.im/

### Encryption

Invitation is encrypted and the entire video and audio streams are also encrypted (source: <https://community.jitsi.org/t/is-jitsi-safe-enough-for-therapy/18967/2>), but only between client and server; Jitsi does not provide E2E encryption, unless under certain circumstances and only as a by-product of an architecture designed to reduce server load (see <https://github.com/jitsi/jitsi-meet/issues/409>).

Https is used for the signalling part, while as for the media part the WebRTC browswer API is used; if there are 2 participants, audio/video stream can be P2P under certain circumstances (with a E2E encryption); with more than 2 participants, **the video streams through the server** (jitsi-videobridge) and at that point it cannot be encrypted.

(source: <https://community.jitsi.org/t/is-jitsi-safe-enough-for-therapy/18967/4> and <https://github.com/jitsi/jitsi-meet/issues/409>)

Jitsi supports [perfect forward secrecy] (source: <https://community.jitsi.org/t/information-about-jitsi/15426/4>).

[perfect forward secrecy]: https://en.wikipedia.org/wiki/Perfect_forward_secrecy

WebRTC security is discussed here: <https://tools.ietf.org/html/draft-ietf-rtcweb-security-arch-19>
