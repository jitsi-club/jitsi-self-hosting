# Problems and proposed solutions

>**DISCLAIMER**: this is not legal nor technical advice. It is just a WIP proposal, open to discussion, on how a Jitsi-Meet server can be quickly (and legally) deployed in order to host one's own private videomeetings, based on authors' practical experiences.

## Jitsi standard deployment solution: critical issues for small providers

The standard deployment of Jitsi (as the one freely offered at <https://meet.jit.si>)  does *not* have any authentication enabled by default: anyone out there in the Internet can create rooms and invite people, and rooms are not password-protected so anyone can join any meeting.

If Jitsi were an end-to-end encrypted conferencing system, the lack of authentication and of access control would be a relative problem for small Jitsi service providers' liability ("small providers" meaning teachers, schools, professionals, businesses, etc.), because they would be completely agnostic with respect to private conversations' content.

But Jitsi does *not* provide E2E encryption -- or rather, it provides it sometimes, under certain conditions (videomeetings between only 2 people, and only when peer-to-peer communication between the 2 people's browsers is actually possible), but only as a mere by-product of an architecture designed to other purposes, i.e. reducing server load (see <https://github.com/jitsi/jitsi-meet/issues/409>).

In Jitsi, communication is always encrypted between client and server, but within the server the audio/video streams pass-through flows unencrypted. Moreover, while Jitsi as such does not store any personal data (name, email, etc.) shared among meeting participants, it does run behind a webserver that processes and logs (and *does have* to log, for security reasons) client connection metadata (IP address, browser used, rooms client is connecting to, etc.) in a way that may make conference participants identifiable (so the GDPR comes in, anyway...).

Therefore, providing a Jitsi service that can be potentially used by anyone in the Internet (by using its standard out-of-the-box deployment solution) involves relevant privacy and security issues, that cannot be handled (and it would not make sense that they were handled) by small decentralized service providers (teachers, schools, professionals, businesses, etc.).

## Proposed implementation and legal compliance

The proposed Jitsi implementation requires authentication for the host, but not for guests. More precisely, in the proposed implementation, while hosts need to have an account to create rooms, guests do not need to have an account, but they should be required to provide a room-specific password to enter. This is to minimize privacy and security issues while keeping the system practical and easy to use for everybody.

Official documentation about Jitsi's built-in authentication suggests to set user passwords through a command that can be run only by sysadmin; since this is not GDPR-compliant, in the [howto](../01-deployment-howto/01-authentication.md) a simple solution is described to allow users to autonomously change their password.

The goal of this project is also to offer some hints and templates for the compliance with privacy regulations with respect to the proposed Jitsi implementation. Obviously, this guide does not cover **any further changes made by the deployer on its own (chat logs, video recordings, Google Analytics)**.

>**NOTE** Logging chat and recording audio/video without being a participant, or without other attendees' knowledge/consent, may constitute a crime.
