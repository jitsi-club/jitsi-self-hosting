# Jitsi Deployment How-To

>**DISCLAIMER**: this is not legal nor technical advice. It is just a WIP proposal, open to discussion, on how a Jitsi server can be quickly (and legally) deployed in order to host one's own private videomeetings, based on authors' practical experiences.

Jitsi official quick-install how-to covers only a very **basic installation**, which **may not be deemed as privacy compliant** (no authentication enabled, no privacy policy regarding personal data processed to run the service[^metadata]).

  [^metadata]: see [platform description](../00-introduction/00-platform-description.md)

Moreover, since **authentication is managed through an external component** (Prosody), **Jitsi documentation does not cover it**, so one has to look at Prosody documentation to figure out how users can change their password set by sysadmin -- which is a fundamental requirement for privacy compliance -- or how to install non-self-signed certificates for prosody -- which are necessary to remotely manage user accounts in a secure way; and so on.

Finally, some important tuning settings (e.g. video management settings to make Jitsi usable by teams of more than 6/10 people) may be difficult to find, since relevant information is splitted among Jitsi subcomponents' repos.

This how-to tries to fill gaps and put together some sparse pieces of Jitsi and Prosody official documentation, in order to get a self-hosted videoconferencing solution that can compete with mainstream proprietary solutions and that can be privacy compliant, too.
